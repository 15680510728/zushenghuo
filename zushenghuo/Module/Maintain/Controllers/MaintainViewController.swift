//
//  MaintainViewController.swift
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class MaintainViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViews()
    }
    
    func setUpViews(){
        self.title = "故障保修"
        self.addNaviBarRightButton(nil, title: "新增") { [weak self] in
            self?.navigationController?.pushViewController(UIStoryboard(name: "Maintain", bundle: nil).instantiateViewControllerWithIdentifier("NewMaintainViewController"), animated: true)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
