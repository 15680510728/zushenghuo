//
//  NewMaintainViewController.swift
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class NewMaintainViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var images:[UIImage!]?{
        didSet{
            tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 1, inSection: 0)], withRowAnimation: .None)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViews()
        images = []
    }
   
    func setUpViews(){
        self.title = "我要保修"
        self.addNaviBarRightButton(nil, title: "提交") { [weak self] in
            self?.showAlertView("提交", message: nil, custiomView: nil, cancelButtonTitle: "确定", otherButtonTitles: nil, onButtonTouchUpInside: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AddMaintainTextViewTableViewCell") as! AddMaintainTextViewTableViewCell
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AddMaintainImageViewTableViewCell") as! AddMaintainImageViewTableViewCell
            cell.images = images
            cell.setActions(addImageAction: {
                //添加照片
                [weak self] in
                let sheet = IBActionSheet(title: nil, callback: {
                    [weak self]
                    (sheet:IBActionSheet!, index:Int) -> Void in
                    if index == 0 {
                        
                        let pvc:UIImagePickerController = MyImagePicker.sharedMyImagePicker().pickImageWithImagePickType(.Camera, completeAction: {
                            [weak self]
                            (image:UIImage!) -> Void in
                            self?.images?.append(image)
                        })
                        self?.presentViewController(pvc, animated: true, completion: nil)
                    }else if index == 1 {
                        let pvc:UIImagePickerController = MyImagePicker.sharedMyImagePicker().pickImageWithImagePickType(.PhotoLibrary, completeAction: {
                            [weak self]
                            (image:UIImage!) -> Void in
                            self?.images?.append(image)
                        })
                        self?.presentViewController(pvc, animated: true, completion: nil)
                    }
                    
                    }, cancelButtonTitle: "取消", destructiveButtonTitle: nil, otherButtonTitlesArray: ["拍照","相册"])
                sheet.showInView(self?.view)
                
                }, seletedImageAction: {
                    //选择了照片
                    (index:Int) -> Void in
                    dlog("选择了第\(index)张照片")
            })
            return cell
        }else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AddMaintainSimpleTextTableViewCell") as! AddMaintainSimpleTextTableViewCell
            cell.textBt.setAttributedTitle(NSAttributedString(string: "景玉阳光小区", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(CGFloat(FONT_SIZE_NORMAL)) , NSForegroundColorAttributeName:GRAY1_TEXT_COLOR]), forState: .Normal)
            cell.textBt.setImage(UIImage(named: "houseIcon"), forState: .Normal)
            return cell
        }else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AddMaintainSimpleTextTableViewCell") as! AddMaintainSimpleTextTableViewCell
            cell.textBt.setAttributedTitle(NSAttributedString(string: "北京市东花市北里20号楼6单元501室", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(CGFloat(FONT_SIZE_NORMAL)) , NSForegroundColorAttributeName:GRAY2_TEXT_COLOR]), forState: .Normal)
            cell.textBt.setImage(UIImage(named: "locationIcon"), forState: .Normal)
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 150
        }else if indexPath.row == 1 {
            return 88
        }else{
            return 44
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
