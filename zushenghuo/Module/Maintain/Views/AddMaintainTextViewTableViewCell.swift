//
//  AddMaintainTextViewTableViewCell.swift
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class AddMaintainTextViewTableViewCell: UITableViewCell,UITextViewDelegate {
    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.font = UIFont.systemFontOfSize(15)
        textView.textColor = GRAY2_TEXT_COLOR
        textView.text = "说说保修的问题吧…"
        self.backgroundColor = BACKGROUND_CENTER_COLOR
        textView.backgroundColor = BACKGROUND_CENTER_COLOR
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "说说保修的问题吧…"{
            textView.text = ""
            textView.textColor = GRAY1_TEXT_COLOR
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.characters.count < 1 {
            textView.textColor = GRAY2_TEXT_COLOR
            textView.text = "说说保修的问题吧…"
        }
    }
}
