//
//  AddMaintainImageViewTableViewCell.swift
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class AddMaintainImageViewTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    private var addAction:(() -> Void)?
    private var imageSeletedAction:((Int) -> Void)?
    var images:[UIImage!]?{
        didSet{
            collectionView.reloadData()
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setActions(addImageAction addImageAction:(() -> Void)? , seletedImageAction:((Int) -> Void)?){
        addAction = addImageAction
        imageSeletedAction = seletedImageAction
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if let ims = images {
            return ims.count + 1
        }else{
            return 1;
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell:AddMaintainImageCollectionCell =  collectionView.dequeueReusableCellWithReuseIdentifier("AddMaintainImageCollectionCell", forIndexPath: indexPath) as! AddMaintainImageCollectionCell
        if let ims = images {
            if indexPath.row == ims.count {
                cell.imageView.image = UIImage(named: "addImage")
            }else{
                cell.imageView.image = ims[indexPath.row]
            }
        }else{
            cell.imageView.image = UIImage(named: "addImage")
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        return CGSizeMake(80, 80)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(4, 0, 0, 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 10
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let ims = images {
            if indexPath.row == ims.count {
                addAction?()
            }else{
                imageSeletedAction?(indexPath.row)
            }
        }else{
            addAction?()
        }
    }

}

class AddMaintainImageCollectionCell:UICollectionViewCell{
    @IBOutlet weak var imageView: UIImageView!
    
}