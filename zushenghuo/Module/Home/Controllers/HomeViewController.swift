//
//  HomeViewController.swift
//  zushenghuo
//
//  Created by David on 15/12/20.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,HomeCollectionViewCellDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl:DDPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViews()
//        if OtherTools.didLoginExpiration(){
//            let nvc = BaseNavigationController(rootViewController: UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("LoadViewController"))
//            self.parentViewController?.presentViewController(nvc, animated: true, completion: nil);
//        }
    }
    
    func setUpViews(){
        collectionView.backgroundColor = MAIN_TINT_COLOR
        pageControl.drwaForZuShenghuoStyle = true
        pageControl.numberOfPages = 3
        pageControl.pageIndicatorTintColor = DIVIDER_COLOR
        pageControl.currentPageIndicatorTintColor = MAIN_TINT_COLOR
        pageControl.userInteractionEnabled = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:CollectionViewDelegate
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell:HomeCollectionViewCell =  collectionView.dequeueReusableCellWithReuseIdentifier("HomeCollectionViewCell", forIndexPath: indexPath) as! HomeCollectionViewCell
        cell.delegate = self
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        return CGSizeMake(UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        self.pageControl.currentPage = Int(scrollView.contentOffset.x / SCREEN_WIDTH)
    }
    
     //MARK:HomeCollectionViewCellDelegate
    func homeCollectionViewCell(homeCollectionViewCell: HomeCollectionViewCell, didSelectedRole roleIndex: Int) {
        dlog("角色导航")
        if roleIndex == 0 {
            self.showAlertView("确认电话呼叫房东", message: "周房东\n电话号码:112", custiomView: nil, cancelButtonTitle: "取消", otherButtonTitles: ["确认"]) {[weak self] (buttonIndex) -> (Void) in
                if buttonIndex == 1 {
                    self?.makeCall("112")
                }
            }
        }else if roleIndex == 1{
            self.navigationController?.pushViewController(UIStoryboard(name: "Me", bundle: nil).instantiateViewControllerWithIdentifier("MeViewController"), animated: true)
        }else if roleIndex == 2{
            self.showAlertView("确认电话呼叫中介", message: "中介\n电话号码:112", custiomView: nil, cancelButtonTitle: "取消", otherButtonTitles: ["确认"]) {[weak self] (buttonIndex) -> (Void) in
                if buttonIndex == 1 {
                    self?.makeCall("112")
                }
            }
        }
    }
    func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedTenancy button:UIButton){
        dlog("租期")
    }
    func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedRentRemind button:UIButton){
        dlog("剩余租期")
    }
    func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedRentPledge button:UIButton){
        dlog("押金")
    }
    func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedWater_gas_electricity_charge button:UIButton){
        let viewController = UIStoryboard(name: "Bill", bundle: nil).instantiateViewControllerWithIdentifier("PublicBillsViewController") as! PublicBillsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedAskLandLord button:UIButton){
        dlog("租期")
       
    }
    func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedMaintain button:UIButton){
        dlog("故障保修")
        self.navigationController?.pushViewController(UIStoryboard(name: "Maintain", bundle: nil).instantiateViewControllerWithIdentifier("MaintainViewController"), animated: true)
    }
    func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedFunction button:UIButton){
        dlog("功能按钮")
    }
}

