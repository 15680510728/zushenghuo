//
//  ArchView.swift
//  zushenghuo
//
//  Created by David on 15/12/20.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class ArrowView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clearColor()
    }
    
    override func drawRect(rect: CGRect) {
        
        let context:CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextMoveToPoint(context, 0, 0)
        CGContextAddLineToPoint(context,SCREEN_WIDTH/2,0)
        CGContextAddLineToPoint(context,SCREEN_WIDTH/2 - 10, self.frame.size.height)
        CGContextAddLineToPoint(context, 0, self.frame.size.height)
        MAIN_TINT_COLOR.setFill()
        CGContextDrawPath(context, .Fill)
        
        CGContextMoveToPoint(context, SCREEN_WIDTH/2, 0)
        CGContextAddLineToPoint(context,SCREEN_WIDTH/2 + 10,self.frame.size.height)
        CGContextAddLineToPoint(context,SCREEN_WIDTH, self.frame.size.height)
        CGContextAddLineToPoint(context, SCREEN_WIDTH, 0)
        MAIN_TINT_COLOR.setFill()
        CGContextDrawPath(context, .Fill)
        

    }
}
