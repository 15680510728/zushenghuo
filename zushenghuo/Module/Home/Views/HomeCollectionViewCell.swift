//
//  HomeCollectionViewCell.swift
//  zushenghuo
//
//  Created by David on 15/12/20.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit
import iCarousel

@objc protocol HomeCollectionViewCellDelegate{
    optional func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedRole roleIndex:Int)
    optional func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedTenancy button:UIButton)
    optional func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedRentRemind button:UIButton)
    optional func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedRentPledge button:UIButton)
    optional func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedWater_gas_electricity_charge button:UIButton)
    optional func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedAskLandLord button:UIButton)
    optional func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedMaintain button:UIButton)
    optional func homeCollectionViewCell(homeCollectionViewCell:HomeCollectionViewCell , didSelectedFunction button:UIButton)
}

class HomeCollectionViewCell: UICollectionViewCell,iCarouselDataSource,iCarouselDelegate {
    //角色导航
    @IBOutlet weak var roleNaviBar: iCarousel!
    //房屋信息
    @IBOutlet weak var houseImage: UIImageView!
    @IBOutlet weak var houseName: UIButton!
    @IBOutlet weak var houseAddress: UIButton!
    //租赁个信息
    @IBOutlet weak var tenancy: UIButton!
    @IBOutlet weak var rentRemind: UIButton!
    @IBOutlet weak var rentPledge: UIButton!
    //租赁时间段
    @IBOutlet weak var rentDate: UILabel!
    //功能区域
    @IBOutlet weak var water_gas_electricity_charge: UIButton!
    @IBOutlet weak var askLandLord: UIButton!
    @IBOutlet weak var maintain: UIButton!
    //功能按钮
    @IBOutlet weak var functionBt: UIButton!
    
    var delegate:HomeCollectionViewCellDelegate?
    
    //当前的View
    var view: UICollectionViewCell!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        view = loadViewFromNib()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        view = loadViewFromNib()
    }
    
    func loadViewFromNib() -> UICollectionViewCell {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: nibName(), bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UICollectionViewCell
        
        view.frame = bounds
        view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.addSubview(view);
        return view
    }
    
    private func nibName() -> String {
        return self.dynamicType.description().componentsSeparatedByString(".").last!
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        roleNaviBar.dataSource = self
        roleNaviBar.delegate = self
        roleNaviBar.type = .Custom
        roleNaviBar.pagingEnabled = true
        roleNaviBar.scrollEnabled = false
        roleNaviBar.scrollToItemAtIndex(1, animated: false)
        roleNaviBar.itemViewAtIndex(0)?.alpha = 0.5
        roleNaviBar.itemViewAtIndex(2)?.alpha = 0.5
        
        functionBt.layer.cornerRadius = 4
        functionBt.layer.masksToBounds = true
        
        tenancy.addTarget(self, action: "tenancyAction:", forControlEvents: .TouchUpInside)
        rentRemind.addTarget(self, action: "rentRemindAction:", forControlEvents: .TouchUpInside)
        rentPledge.addTarget(self, action: "rentPledgeAction:", forControlEvents: .TouchUpInside)
        water_gas_electricity_charge.addTarget(self, action: "water_gas_electricity_chargeAction:", forControlEvents: .TouchUpInside)
        askLandLord.addTarget(self, action: "askLandLordAction:", forControlEvents: .TouchUpInside)
        maintain.addTarget(self, action: "maintainAction:", forControlEvents: .TouchUpInside)
        functionBt.addTarget(self, action: "functionBtAction:", forControlEvents: .TouchUpInside)
        
    }
    //MARK:carouselDelegate
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return 3
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView{
        var itemView: RoleHeaderView
        
        if (view == nil){
            itemView = RoleHeaderView(frame: CGRectMake(0, 0, 90, 90))
        }else{
            itemView = view as! RoleHeaderView
        }
        return itemView
    }
    
    func carousel(carousel: iCarousel, itemTransformForOffset offset: CGFloat, baseTransform transform: CATransform3D) -> CATransform3D {
        
        let max_sacle:CGFloat = 1.0
        let min_scale:CGFloat = 0.6
        var mytrans:CATransform3D!
        if offset <= 1 && offset >= -1 {
            let tempScale:CGFloat = offset < 0 ? 1+offset : 1-offset
            let slope:CGFloat = (max_sacle - min_scale) / 1
            
            let scale:CGFloat = min_scale + (slope * tempScale);
            mytrans = CATransform3DScale(transform, scale, scale, 1);
        }else{
            mytrans = CATransform3DScale(transform, min_scale, min_scale, 1);
        } 
        return CATransform3DTranslate(mytrans, offset * ( self.roleNaviBar.itemWidth + (SCREEN_WIDTH / 6) ) * 1.4, 0.0, 0.0);
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        for view in carousel.visibleItemViews as! [UIView]{
            view.alpha = 0.5
        }
        carousel.itemViewAtIndex(index)?.alpha = 1
    }
    
    func carousel(carousel: iCarousel, shouldSelectItemAtIndex index: Int) -> Bool {
        delegate?.homeCollectionViewCell?(self, didSelectedRole: index)
        return false
    }
    
    //MARK:private method
    func tenancyAction(sender:UIButton){
        delegate?.homeCollectionViewCell?(self, didSelectedTenancy: sender)
    }
    func rentRemindAction(sender:UIButton){
        delegate?.homeCollectionViewCell?(self, didSelectedRentRemind: sender)
    }
    func rentPledgeAction(sender:UIButton){
        delegate?.homeCollectionViewCell?(self, didSelectedRentPledge: sender)
    }
    func water_gas_electricity_chargeAction(sender:UIButton){
        delegate?.homeCollectionViewCell?(self, didSelectedWater_gas_electricity_charge: sender)
    }
    func askLandLordAction(sender:UIButton){
        delegate?.homeCollectionViewCell?(self, didSelectedAskLandLord: sender)
    }
    func maintainAction(sender:UIButton){
        delegate?.homeCollectionViewCell?(self, didSelectedMaintain: sender)
    }
    func functionBtAction(sender:UIButton){
        delegate?.homeCollectionViewCell?(self, didSelectedFunction: sender)
    }
}
