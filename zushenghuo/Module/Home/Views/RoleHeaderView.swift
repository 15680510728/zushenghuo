//
//  RoleHeaderView.swift
//  zushenghuo
//
//  Created by David on 15/12/20.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class RoleHeaderView: UIView {
    @IBOutlet weak var header: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var msgNum: UILabel!
    @IBOutlet weak var tips: UILabel!
    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        view = loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        view = loadViewFromNib()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: nibName(), bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        view.frame = bounds
        view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.addSubview(view);
        msgNum.layer.cornerRadius = msgNum.frame.size.height/2
        msgNum.layer.masksToBounds = true
        tips.layer.cornerRadius = tips.frame.size.height/2
        tips.layer.masksToBounds = true
        header.layer.cornerRadius = header.frame.size.height/2
        header.layer.masksToBounds = true
        header.layer.borderColor = UIColor.whiteColor().CGColor
        header.layer.borderWidth = 2
        return view
    }
    
    private func nibName() -> String {
        return self.dynamicType.description().componentsSeparatedByString(".").last!
    }
    
}
