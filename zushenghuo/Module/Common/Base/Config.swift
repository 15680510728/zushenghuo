//
//  ZMConfig.swift
//  zhangmai
//
//  Created by 戴伟来 on 15/12/3.
//  Copyright © 2015年 zhangmai. All rights reserved.
//

import Foundation
import UIKit


#if DEBUG
    func dlog<T>(t:T){print(t)}
#else
    func dlog<T>(t:T){}
#endif
let kAppID = "wx9b967001f3cdf3d4"
let kAppSecret = "77c84bcd69c2db13f9b677d419073b05"
let kAccessToken = "AccessToken"
let kOpenID = "OpenID"



//MARK:登录有效期
let kExpiresDate = "expiresdate"

//MARK:第三方登录和支付
let kLoginNotification = "loginnotification"
let kPaySuccessNotification = "paysuccessnotification"
let kPayFailedNotification = "payfailednotification"
//MARK:颜色
let MAIN_TINT_COLOR = UIColorFromRGB(0x3490F7)
let VICE_MAIN_COLOR = UIColorFromRGB(0x519FF7)
let VICE_RED_COLOR = UIColorFromRGB(0xFD6B5C)
let VICE_BACKGROUND_COLOR = UIColorFromRGB(0xF5F6FA)
let VICE_GREEN_COLOR = UIColorFromRGB(0x4EBB98)

let DIVIDER_COLOR = UIColorFromRGB(0xEBEBEB)
let GRAY1_TEXT_COLOR = UIColorFromRGB(0x575757)//字体颜色 深灰
let GRAY2_TEXT_COLOR = UIColorFromRGB(0x999999)//字体颜色 浅灰
let GRAY3_TEXT_COLOR = UIColorFromRGB(0xCDCDCD)//字体颜色 最浅
let BACKGROUND_CENTER_COLOR = UIColorFromRGB(0xF6F6F6)//中间背景


//MARK: 尺寸
let FONT_SIZE_SMALL = 12
let FONT_SIZE_NORMAL = 15
let FONT_SIZE_BIG = 18

let SCREEN_WIDTH = UIScreen.mainScreen().bounds.width
let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.height

//方法
func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}