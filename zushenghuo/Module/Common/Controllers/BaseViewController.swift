//
//  BaseViewController.swift
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit
import SVProgressHUD
import ActionKit


class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = BACKGROUND_CENTER_COLOR
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
    }
    
    deinit{
        dlog("=======\(NSStringFromClass(self.dynamicType))被析构=======")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addNaviBarRightButton(image:UIImage?,title:String? ,closure: () -> ()){
        
        let bt = UIButton(frame: CGRectMake(0, 0, 20, 20))
        if let i = image{
            bt.setImage(i, forState: .Normal)
        }else{
            var width = NSString(string: title!).boundingRectWithSize(CGSizeMake(SCREEN_WIDTH, 20), options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(15)], context: nil).size.width
            if width > 60{
                width = 60
            }
            bt.frame = CGRectMake(0, 0, width, 20)
            bt.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            bt.titleLabel?.font = UIFont.systemFontOfSize(15)
            bt.setTitle(title, forState: .Normal)
        }
        let barItem = UIBarButtonItem(customView: bt)
        bt.addControlEvent(.TouchUpInside, closure: closure)
        self.navigationItem.rightBarButtonItem = barItem
    }
    
    func makeCall(number:String!){
        UIApplication.sharedApplication().openURL(NSURL(string: "tel://\(number)")!)
    }

    func showAlertView(title:String? , message:String? ,custiomView:UIView?,cancelButtonTitle: String! ,otherButtonTitles: [String]? , onButtonTouchUpInside:((buttonIndex: Int) -> (Void))?){
        let alert:SwiftAlertView!
        if let view = custiomView{
            alert = SwiftAlertView(contentView: view, delegate: nil, cancelButtonTitle: cancelButtonTitle, otherButtonTitles: otherButtonTitles)
        }else{
            alert = SwiftAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: cancelButtonTitle, otherButtonTitles: otherButtonTitles)
        }
        alert.show()
        alert.clickedButtonAction = onButtonTouchUpInside
    }
    
    func showToast(info message:String){
        SVProgressHUD.setFont(UIFont.systemFontOfSize(15))
        SVProgressHUD.showInfoWithStatus(message)
    }
    
    func showHUD(){
        SVProgressHUD.show()
    }
    
    func hideHUD(){
        SVProgressHUD.dismiss()
    }
}
