//
//  BaseNavigationController.swift
//  zhangmai
//
//  Created by 戴伟来 on 15/12/4.
//  Copyright © 2015年 zhangmai. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = MAIN_TINT_COLOR
        self.navigationBar.titleTextAttributes = [NSFontAttributeName:UIFont.systemFontOfSize(18),NSForegroundColorAttributeName:UIColor.whiteColor()]
    }
    
    override func pushViewController(viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        let backBt = UIButton(frame: CGRectMake(0, 0, 20, 20))
        backBt.setImage(UIImage(named: "back"), forState: .Normal)
        backBt.addTarget(self, action: "customPop", forControlEvents: UIControlEvents.TouchUpInside)
        let barItem = UIBarButtonItem(customView: backBt)
        viewController.navigationItem.leftBarButtonItem = barItem
    }
    
    func customPop(){
        self.popViewControllerAnimated(true)
    }
}
