//
//  MyQRTableViewCell.swift
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class MyQRTableViewCell: UITableViewCell {

    @IBOutlet weak var qrView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        qrView.layer.cornerRadius = 10;
        qrView.layer.masksToBounds = true
        self.backgroundColor = UIColorFromRGB(0x323135)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
