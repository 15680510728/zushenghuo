//
//  AboutViewController.swift
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class AboutViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "关于"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BillHistoryTableViewCell") as! BillHistoryTableViewCell
        if indexPath.row == 0 {
            cell.itemWithDate.text = "服务电话"
            cell.itemMoney.hidden = false
            cell.itemMoney.text = "800-345-566"
            return cell
        }else if indexPath.row == 1{
            cell.itemWithDate.text = "团队介绍"
            cell.itemMoney.hidden = true
            return cell
        }else{
            cell.itemWithDate.text = "负责声明"
            cell.itemMoney.hidden = true
            return cell
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UINib(nibName: "AboutHeaderView", bundle: nil).instantiateWithOwner(self, options: nil).first as! AboutHeaderView
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UINib(nibName: "AboutFooterView", bundle: nil).instantiateWithOwner(self, options: nil).first as! AboutFooterView
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            self.showAlertView("联系客服", message: "确定拨打 800-345-566？", custiomView: nil, cancelButtonTitle: "取消", otherButtonTitles:["确定"], onButtonTouchUpInside: {
                [weak self]
                (buttonIndex) -> (Void) in
                if buttonIndex == 1 {
                    self?.makeCall("800345566")
                }
            })
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44;
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return SCREEN_HEIGHT/2
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20;
    }
    

}
