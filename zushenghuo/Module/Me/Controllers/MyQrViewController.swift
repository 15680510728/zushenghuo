//
//  MyQrViewController.swift
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class MyQrViewController: BaseViewController, UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "我的二维码名片"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColorFromRGB(0x323135)
        self.tableView.backgroundColor = UIColorFromRGB(0x323135)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCellWithIdentifier("MyQRTableViewCell") as! MyQRTableViewCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return SCREEN_HEIGHT - 100
    }

}
