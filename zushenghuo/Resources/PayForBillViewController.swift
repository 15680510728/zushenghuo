//
//  PayForBillViewController.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class PayForBillViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = VICE_BACKGROUND_COLOR
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "paySuccess", name: kPaySuccessNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "payFailed", name: kPayFailedNotification, object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 71
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BillItemTableViewCell", forIndexPath: indexPath) as! BillItemTableViewCell
        if indexPath.section == 0 {
            cell.itemIcon.image = UIImage(named: "Shape_alipay")
            cell.itemName.text = "支付宝支付"
        }else{
            cell.itemIcon.image = UIImage(named: "Shape_wechat")
            cell.itemName.text = "微信支付"
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            //支付宝支付
        }else{
            //微信支付
            //1需要先向服务器提交订单
            //2服务器返回微信订单信息
            //3发起支付请求
            wechatPay(params: ["":""])
        }
    }
    private func wechatPay(params orderInfo:NSDictionary?) {
        guard let _ = orderInfo!.objectForKey("retcode") else{
            let stamp = orderInfo!.objectForKey("timestamp")
            let req = PayReq()
            req.partnerId = orderInfo!.objectForKey("partnerid") as! String
            req.prepayId = orderInfo!.objectForKey("prepayid") as! String
            req.nonceStr = orderInfo!.objectForKey("noncestr") as! String
            req.timeStamp = UInt32(stamp!.intValue)
            req.package = orderInfo!.objectForKey("package") as! String
            req.sign = orderInfo!.objectForKey("sign") as! String
            WXApi.sendReq(req)
            return
        }
    }
    
    @objc private func paySuccess(){
        
    }
    @objc private func payFailed(){
       
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kPayFailedNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kPaySuccessNotification, object: nil)
        dlog("good！Notification被移除")
    }
}
