//
//  BillItemDetailTableViewCell.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class BillItemDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var itemMoney: UILabel!
    @IBOutlet weak var itemKind: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var billIdentifer: UILabel!
    @IBOutlet weak var billCompany: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemMoney.textColor = VICE_GREEN_COLOR
    }
}
