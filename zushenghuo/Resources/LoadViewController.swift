//
//  LoadViewController.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/21.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class LoadViewController: BaseViewController {

    @IBOutlet weak var zushenghuoLabel: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var explainLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupViews()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    //设置view
    private func setupViews(){
        registerButton.titleLabel?.textColor = VICE_MAIN_COLOR
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func registeredButtonClick(sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Login", bundle: nil)
        let controller = storyBoard.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func loginButtonClick(sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Login", bundle: nil)
        let controller = storyBoard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
