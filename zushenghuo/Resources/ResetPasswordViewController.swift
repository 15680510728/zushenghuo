//
//  ResetPasswordViewController.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/21.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift

class ResetPasswordViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var userMobileNumber:String = "" {
        didSet{
            tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 1, inSection: 0)] , withRowAnimation: UITableViewRowAnimation.None)
        }
    }

    var verificationCode:String = ""
    var newPassword:String = ""
    var repetPassword:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
    }
    private func setupViews(){
        tableView.backgroundColor = VICE_BACKGROUND_COLOR
        let footerView = TableViewFooterView.initMyTableViewFooterView(buttonTitle: "完成", buttonColor: VICE_MAIN_COLOR) {[weak self] () -> Void in
            if let strongSelf = self {
                IQKeyboardManager.sharedManager().resignFirstResponder()
                if strongSelf.userMobileNumber.isEmpty {
                    strongSelf.showToast(info: "请输入手机号")
                }else{
                    if OtherTools.isMobileNumber(mobileNumber: strongSelf.userMobileNumber){
                        if strongSelf.verificationCode.isEmpty {
                            strongSelf.showToast(info: "请输入验证码")
                        }else{
                            if strongSelf.newPassword.isEmpty {
                                strongSelf.showToast(info: "请输入新密码")
                            }else{
                                if OtherTools.isPasswordFormate(password: strongSelf.newPassword){
                                    if strongSelf.repetPassword.isEmpty {
                                        strongSelf.showToast(info: "请重复新密码")
                                    }else{
                                        if strongSelf.newPassword == strongSelf.repetPassword {
                                            SVProgressHUD.showSuccessWithStatus("验证成功")
                                        }else{
                                            strongSelf.showToast(info: "两次密码不一致")
                                        }
                                    }
                                }else{
                                    strongSelf.showToast(info: "请正确输入密码")
                                }
                            }
                        }
                    }else{
                        strongSelf.showToast(info: "请正确输入手机号")
                    }
                }
            }
        }
        tableView.tableFooterView = footerView
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let tableViewCell = tableView.dequeueReusableCellWithIdentifier("SingleTextFieldTableViewCell", forIndexPath: indexPath) as! SingleTextFieldTableViewCell
                tableViewCell.contentTextField.placeholder = "请输入手机号"
                tableViewCell.contentTextField.keyboardType = UIKeyboardType.NumberPad
                tableViewCell.contentTextField.tag = 1
                tableViewCell.contentTextField.addTarget(self, action: "textFieldWithText:", forControlEvents: UIControlEvents.EditingChanged)
                return tableViewCell
            }else{
                let tableViewCell = tableView.dequeueReusableCellWithIdentifier("TextFieldAndButtonTableViewCell", forIndexPath: indexPath) as! TextFieldAndButtonTableViewCell
                tableViewCell.contentTextField.placeholder = "请输入验证码"
                tableViewCell.contentTextField.keyboardType = UIKeyboardType.NumberPad
                tableViewCell.contentTextField.text = verificationCode
                tableViewCell.phoneNumber = userMobileNumber
                tableViewCell.contentTextField.tag = 2
                tableViewCell.contentTextField.addTarget(self, action: "textFieldWithText:", forControlEvents: UIControlEvents.EditingChanged)
                return tableViewCell
            }
        }else{
            var tableViewCell:SingleTextFieldTableViewCell
            if indexPath.row == 0{
                tableViewCell = tableView.dequeueReusableCellWithIdentifier("SingleTextFieldTableViewCell", forIndexPath: indexPath) as! SingleTextFieldTableViewCell
                tableViewCell.contentTextField.placeholder = "请输入新密码"
                tableViewCell.contentTextField.secureTextEntry = true
                tableViewCell.contentTextField.keyboardType = UIKeyboardType.Alphabet
                tableViewCell.contentTextField.tag = 3
                tableViewCell.contentTextField.addTarget(self, action: "textFieldWithText:", forControlEvents: UIControlEvents.EditingChanged)
            }else{
                tableViewCell = tableView.dequeueReusableCellWithIdentifier("SingleTextFieldTableViewCell", forIndexPath: indexPath) as! SingleTextFieldTableViewCell
                tableViewCell.contentTextField.placeholder = "请重复新密码"
                tableViewCell.contentTextField.secureTextEntry = true
                tableViewCell.contentTextField.keyboardType = UIKeyboardType.Alphabet
                tableViewCell.contentTextField.tag = 4
                tableViewCell.contentTextField.addTarget(self, action: "textFieldWithText:", forControlEvents: UIControlEvents.EditingChanged)
            }
            return tableViewCell
        }
    }
    
    @objc private func textFieldWithText(textField:UITextField){
        let text = textField.text
        switch textField.tag {
        case 1:
            userMobileNumber = text!
        case 2:
            verificationCode = text!
        case 3:
            newPassword = text!
        case 4:
            repetPassword = text!
        default:
            break
        }
    }
}
