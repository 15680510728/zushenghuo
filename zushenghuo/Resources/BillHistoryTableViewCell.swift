//
//  BillHistoryTableViewCell.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class BillHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var itemWithDate: UILabel!
    @IBOutlet weak var itemMoney: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
