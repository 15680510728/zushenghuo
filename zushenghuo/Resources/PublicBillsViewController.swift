//
//  PublicBillsViewController.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class PublicBillsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var mainTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    private func setupViews() {
        let rightBarButton = UIButton(frame: CGRectMake(0, 0, 60, 24))
        rightBarButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        rightBarButton.setTitle("缴费历史", forState: UIControlState.Normal)
        rightBarButton.addTarget(self, action: "checkoutBillHistory", forControlEvents: UIControlEvents.TouchUpInside)
        let rightBarButtonItem = UIBarButtonItem(customView: rightBarButton)
        self.navigationItem.setRightBarButtonItem(rightBarButtonItem, animated: false)
        mainTableView.backgroundColor = VICE_BACKGROUND_COLOR
        let footerView = TableViewFooterView.initMyTableViewFooterView(buttonTitle: "缴费", buttonColor: VICE_RED_COLOR) {[weak self] () -> Void in
            let viewController = UIStoryboard(name: "Bill", bundle: nil).instantiateViewControllerWithIdentifier("PayForBillViewController") as! PayForBillViewController
            self?.navigationController?.pushViewController(viewController, animated: true)
        }
        mainTableView.tableFooterView = footerView
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let monthLabel = UILabel(frame: CGRectMake(0,0,100,35))
        monthLabel.textColor = UIColor.lightGrayColor()
        monthLabel.font = UIFont.systemFontOfSize(12)
        monthLabel.text = "    2015年9月份"
        return monthLabel
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BillItemTableViewCell", forIndexPath: indexPath) as! BillItemTableViewCell
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let viewController = UIStoryboard(name: "Bill", bundle: nil).instantiateViewControllerWithIdentifier("BillItemDetailViewController") as! BillItemDetailViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func checkoutBillHistory() {
        let viewController = UIStoryboard(name: "Bill", bundle: nil).instantiateViewControllerWithIdentifier("BillHistoryViewController") as! BillHistoryViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
