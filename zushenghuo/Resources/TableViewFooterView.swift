//
//  TableViewFooterView.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/21.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit
import SnapKit

class TableViewFooterView: UIView {
    var button:UIButton?
    var action:(() -> Void)?
    
    class func initMyTableViewFooterView(buttonTitle title:String,buttonColor:UIColor,buttonAction:(() -> Void)) -> TableViewFooterView{
        let view = TableViewFooterView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 100))
        view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 100)
        view.backgroundColor = VICE_BACKGROUND_COLOR
        view.action = buttonAction
        view.button = UIButton(frame: CGRectZero)
        view.button!.backgroundColor = buttonColor
        view.button!.setTitle(title, forState: UIControlState.Normal)
        view.button!.titleLabel?.textColor = UIColor.whiteColor()
        view.button!.titleLabel?.font = UIFont.systemFontOfSize(16)
        view.button!.addTarget(view, action: "buttonAction", forControlEvents: UIControlEvents.TouchUpInside)
        view.addSubview(view.button!)
        view.button!.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(40);
            make.width.equalTo(SCREEN_WIDTH - 20);
            make.top.equalTo(view).offset(30)
            make.leading.equalTo(view).offset(10)
        }
        return view
    }
    
    @objc private func buttonAction(){
        action?()
    }
}
