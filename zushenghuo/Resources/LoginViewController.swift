//
//  LoginViewController.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/21.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift

class LoginViewController: BaseViewController {

    @IBOutlet weak var userPhoneTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    private func setupViews(){
        self.view.backgroundColor = VICE_BACKGROUND_COLOR
        loginButton.backgroundColor = VICE_MAIN_COLOR
        registerButton.titleLabel?.textColor = VICE_MAIN_COLOR
        registerButton.layer.borderWidth = 1
        registerButton.layer.borderColor = VICE_MAIN_COLOR.CGColor
        userPhoneTextField.keyboardType = UIKeyboardType.NumberPad
        userPasswordTextField.keyboardType = UIKeyboardType.Alphabet
    }
    @IBAction func forgetPasswordButtonclick(sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Login", bundle: nil)
        let controller = storyBoard.instantiateViewControllerWithIdentifier("ResetPasswordViewController") as! ResetPasswordViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func loginButtonclick(sender: UIButton) {
        IQKeyboardManager.sharedManager().resignFirstResponder()
        let userMobileNumber = userPhoneTextField.text! as String
        let userPassworld = userPasswordTextField.text! as String
        if userMobileNumber.isEmpty {
            showToast(info: "请输入手机号")
        }else{
            if OtherTools.isMobileNumber(mobileNumber: userMobileNumber) {
                if userPassworld.isEmpty {
                    showToast(info: "请输入密码")
                }else{
                    if OtherTools.isPasswordFormate(password: userPassworld) {
                        //在这里执行登录操作
                        SVProgressHUD.showWithStatus("正在登录")
                        self.dismissViewControllerAnimated(true, completion: nil)
                        MyRequest.loginWithPhone(userMobileNumber, password: userPassworld, response: {
                            [weak self]
                            (oper:AFHTTPRequestOperation!, resp:AnyObject!, respstr:String!, code:MyRequestResponseCode) -> Void in
                            if code == MyRequestResponseCode.Success {
                                print(resp)
                                SVProgressHUD.showSuccessWithStatus("登录成功")
                                OtherTools.saveExpiresDate(expiresIn: (resp as! NSDictionary).valueForKey("expires_in")!.doubleValue)
                                self?.dismissViewControllerAnimated(true, completion: nil)
                            }else{
                                self?.showToast(info: respstr)
                            }
                        })
                    }else{
                        showToast(info: "请正确输入密码")
                    }
                }
            }else{
                showToast(info: "请正确输入手机号")
            }
        }
    }
    @IBAction func registerButtonclick(sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Login", bundle: nil)
        let controller = storyBoard.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func weixinLogin(sender: UIButton) {
        let isInstallWX = WXApi.isWXAppInstalled()
        if isInstallWX {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginSuccess:", name: kLoginNotification, object: nil)
            sendAuthRequest()
        }else{
            UIAlertView(title: "温馨提示", message: "您还未安装微信,请使用其它登录方式", delegate: self, cancelButtonTitle: "OK").show()
        }
    }
    private func sendAuthRequest(){
        let req = SendAuthReq()
        req.scope = "snsapi_userinfo"
        req.state = "123"
        WXApi.sendReq(req)
    }
    @objc private func loginSuccess(note:NSNotification){
        let urlStr = "https://api.weixin.qq.com/sns/userinfo?access_token=\(NSUserDefaults.standardUserDefaults().valueForKey(kAccessToken))&openid=\(NSUserDefaults.standardUserDefaults().valueForKey(kOpenID))"
        let url = NSURL(string: urlStr)
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 30.0)
        if (NSURLConnection(request: request, delegate: self) != nil) {
            UIAlertView(title: "温馨提示", message: "登录成功", delegate: self, cancelButtonTitle: "OK").show()
            NSNotificationCenter.defaultCenter().removeObserver(self, name: kLoginNotification, object: nil)
        }else{
            UIAlertView(title: "温馨提示", message: "登录失败", delegate: self, cancelButtonTitle: "OK").show()
            NSNotificationCenter.defaultCenter().removeObserver(self, name: kLoginNotification, object: nil)
        }

    }
}
