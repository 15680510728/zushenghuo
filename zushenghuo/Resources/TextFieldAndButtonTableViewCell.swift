//
//  TextFieldAndButtonTableViewCell.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/21.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit
import SVProgressHUD

class TextFieldAndButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var contentTextField: UITextField!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    var oneMinute:Int = 0
    var timer:NSTimer?
    var phoneNumber:String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.None
        otherButton.titleLabel?.textColor = VICE_MAIN_COLOR
        timerLabel.textColor = GRAY3_TEXT_COLOR
        timerLabel.font = UIFont.systemFontOfSize(15)
        timerLabel.hidden = true
        timerLabel.backgroundColor = UIColor.whiteColor()
        otherButton.addTarget(self, action: "buttonClick", forControlEvents: UIControlEvents.TouchUpInside)
    }
    @objc private func buttonClick(){
        if phoneNumber.isEmpty {
            SVProgressHUD.showInfoWithStatus("请输入手机号")
        }else {
            if OtherTools.isMobileNumber(mobileNumber: phoneNumber) {
                MyRequest.pinWithPhone(phoneNumber, response: { (oper:AFHTTPRequestOperation!, resp:AnyObject!, respstr:String!, code:MyRequestResponseCode) -> Void in
                    
                })
                oneMinute = 60
                timerLabel.text = "重新发送(60s)"
                timerLabel.hidden = false
                timer = NSTimer(timeInterval: 1.0, target: self, selector: "startTimer", userInfo: nil, repeats: true)
                NSRunLoop.currentRunLoop().addTimer(timer!, forMode: NSRunLoopCommonModes)
            }else{
                SVProgressHUD.showInfoWithStatus("手机号格式错误")
            }
        }
    }
    @objc private func startTimer(){
        oneMinute--
        if oneMinute == 0{
            timer?.invalidate()
            timerLabel.hidden = true
        }else{
            timerLabel.text = "重新发送(\(oneMinute)s)"
        }
    }
}
