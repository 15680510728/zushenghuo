//
//  BillItemDetailViewController.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class BillItemDetailViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    private func setupViews(){
        tableView.backgroundColor = VICE_BACKGROUND_COLOR
        let footerView = TableViewFooterView.initMyTableViewFooterView(buttonTitle: "返回首页", buttonColor: VICE_MAIN_COLOR) {[weak self] () -> Void in
            self?.navigationController?.popToRootViewControllerAnimated(true)
        }
        tableView.tableFooterView = footerView
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 211
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BillItemDetailTableViewCell", forIndexPath: indexPath) as! BillItemDetailTableViewCell
        return cell
    }
}
