//
//  RegisterViewController.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/21.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift
class RegisterViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var roleSelectSegment: UISegmentedControl!
    var userMobileNumber:String = "" {
        didSet{
            tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 1, inSection: 0)] , withRowAnimation: UITableViewRowAnimation.None)
        }
    }
    var verificationCode:String = ""
    var newPassword:String = ""
    var repetPassword:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupViews()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
        self.view.backgroundColor = VICE_BACKGROUND_COLOR
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func setupViews() {
        tableView.backgroundColor = VICE_BACKGROUND_COLOR
        let footerView = TableViewFooterView.initMyTableViewFooterView(buttonTitle: "注册并登录", buttonColor: VICE_MAIN_COLOR) {[weak self] () -> Void in
            if let strongSelf = self {
                IQKeyboardManager.sharedManager().resignFirstResponder()
                if strongSelf.userMobileNumber.isEmpty {
                    strongSelf.showToast(info: "请输入手机号")
                }else{
                    if OtherTools.isMobileNumber(mobileNumber: strongSelf.userMobileNumber){
                        if strongSelf.verificationCode.isEmpty {
                            strongSelf.showToast(info: "请输入验证码")
                        }else{
                            if strongSelf.newPassword.isEmpty {
                                strongSelf.showToast(info: "请输入密码")
                            }else{
                                if OtherTools.isPasswordFormate(password: strongSelf.newPassword){
                                    if strongSelf.repetPassword.isEmpty {
                                        strongSelf.showToast(info: "请重复密码")
                                    }else{
                                        if strongSelf.newPassword == strongSelf.repetPassword {
                                            MyRequest.registerWithPhone(strongSelf.userMobileNumber, pin: strongSelf.verificationCode, password: strongSelf.newPassword, response: { (oper:AFHTTPRequestOperation!, resp:AnyObject!, respstr:String!, code:MyRequestResponseCode) -> Void in
                                                if code == MyRequestResponseCode.Success {
                                                     SVProgressHUD.showSuccessWithStatus("注册成功，正在登录")
                                                }else{
                                                    SVProgressHUD.showErrorWithStatus(respstr)
                                                }
                                            })
                                        }else{
                                            strongSelf.showToast(info: "两次密码不一致")
                                        }
                                    }
                                }else{
                                    strongSelf.showToast(info: "请正确输入密码")
                                }
                            }
                        }
                    }else{
                        strongSelf.showToast(info: "请正确输入手机号")
                    }
                }
            }
        }
        tableView.tableFooterView = footerView
        roleSelectSegment.tintColor = VICE_MAIN_COLOR
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let tableViewCell = tableView.dequeueReusableCellWithIdentifier("SingleTextFieldTableViewCell", forIndexPath: indexPath) as! SingleTextFieldTableViewCell
                tableViewCell.contentTextField.placeholder = "请输入手机号"
                tableViewCell.contentTextField.keyboardType = UIKeyboardType.NumberPad
                tableViewCell.contentTextField.addTarget(self, action: "textFieldWithText:", forControlEvents: UIControlEvents.EditingChanged)
                tableViewCell.contentTextField.tag = 1
                return tableViewCell
            }else{
                let tableViewCell = tableView.dequeueReusableCellWithIdentifier("TextFieldAndButtonTableViewCell", forIndexPath: indexPath) as! TextFieldAndButtonTableViewCell
                tableViewCell.contentTextField.placeholder = "请输入验证码"
                tableViewCell.contentTextField.keyboardType = UIKeyboardType.NumberPad
                tableViewCell.contentTextField.text = verificationCode
                tableViewCell.phoneNumber = userMobileNumber
                tableViewCell.contentTextField.addTarget(self, action: "textFieldWithText:", forControlEvents: UIControlEvents.EditingChanged)
                tableViewCell.contentTextField.tag = 2
                return tableViewCell
            }
        }else{
            var tableViewCell:SingleTextFieldTableViewCell
            if indexPath.row == 0{
                tableViewCell = tableView.dequeueReusableCellWithIdentifier("SingleTextFieldTableViewCell", forIndexPath: indexPath) as! SingleTextFieldTableViewCell
                tableViewCell.contentTextField.placeholder = "设置密码"
                tableViewCell.contentTextField.keyboardType = UIKeyboardType.Alphabet
                tableViewCell.contentTextField.secureTextEntry = true
                tableViewCell.contentTextField.addTarget(self, action: "textFieldWithText:", forControlEvents: UIControlEvents.EditingChanged)
                tableViewCell.contentTextField.tag = 3
            }else{
                tableViewCell = tableView.dequeueReusableCellWithIdentifier("SingleTextFieldTableViewCell", forIndexPath: indexPath) as! SingleTextFieldTableViewCell
                tableViewCell.contentTextField.placeholder = "重复密码"
                tableViewCell.contentTextField.keyboardType = UIKeyboardType.Alphabet
                tableViewCell.contentTextField.secureTextEntry = true
                tableViewCell.contentTextField.addTarget(self, action: "textFieldWithText:", forControlEvents: UIControlEvents.EditingChanged)
                tableViewCell.contentTextField.tag = 4
            }
            return tableViewCell
        }
    }
    @IBAction func roleSelectedButtonClick(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            print("我是房客")
        }else{
            print("我是房东")
        }
    }
    @objc private func textFieldWithText(textField:UITextField){
        let text = textField.text
        switch textField.tag {
        case 1:
            userMobileNumber = text!
        case 2:
            verificationCode = text!
        case 3:
            newPassword = text!
        case 4:
            repetPassword = text!
        default:
            break
        }
    }
}
