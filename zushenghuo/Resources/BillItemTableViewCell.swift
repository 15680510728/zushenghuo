//
//  BillItemTableViewCell.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/22.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import UIKit

class BillItemTableViewCell: UITableViewCell {

    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var itemIcon: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemMoney: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
