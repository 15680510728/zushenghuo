//
//  ZhihubaoPayManager.h
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/23.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Order.h"
@interface ZhihubaoPayManager : NSObject
+ (void)PayWithOrder:(Order*)order;
@end
