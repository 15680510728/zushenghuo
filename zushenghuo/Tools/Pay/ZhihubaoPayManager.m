//
//  ZhihubaoPayManager.m
//  zushenghuo
//
//  Created by 戴伟来 on 15/12/23.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

#import "ZhihubaoPayManager.h"
#import <AlipaySDK/AlipaySDK.h>
#import "DataSigner.h"

@implementation ZhihubaoPayManager
+ (void)PayWithOrder:(Order*)order{
    NSString *partner = @"2088121143199623";
    NSString *seller = @"";
    NSString *privateKey = @"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC+062Jvn5SNLrN+lGG1YBUL8WRVMaWpMurwzgPHMOf2Se8TEK/TL1HGw65Z2OqgmHGBgpDf6Qw2C1CGQT/SxR8IoxkiMPPQDG7qQGXArKLOB0FI5fXAGvvXGGBDr7nwAKPC31pi8+D4Q34NEwE/z90dNFKbqHdjXM3JhHYjMsKDmyb5AXqJSfjDZu5YlQ1KZhzDsw8FZ13phdRdrZeR0JAoM/RUpQadbRW2zD33x5vn+EFA7BhRZbtSgC0zUGt6JvnTNsxPtZcaFaovaTJkQ++JTH+dK48zN2WeOvR/sc/Tjw3wQRPdwgu3X86CW3+XwAqRWNpWia8Dmiw4eq3pBlNAgMBAAECggEBAK9GdsticVzImhy/o0qZqDmrd2i52X5c98ZYlZEr9nNIXrsRwO09a014iNm7IUxoXa4pthuxjfpfVZK0sGZda/9pLFLbXB9s80FPRuqRy7UcYWtq+fi9pmZBED4ecTMkpjUFdLgRdxXHDbHBN2IPoRUyh1o6q+p4Fi+9g/7Hs2cTqgZ6P2KFE0CBV8hnjLLnnf8x8Tbv2d0RvZ3hXmsno/LN72qrZIOMC/YAQmtXeKtqz6b31orBZgYqXWUaHLNrmBy+1f9txJF1z8AxjxMbPiGa98CUPtnRkmdCrMztlppcKCkl2OzX8wi3+oARrXr7RCL7BQelj6ENlMsVrtB/EXECgYEA6VWhnJaUsvxPgl7KANSCX0cZYe+GnEWyGVP7tlkHcGTmfKCVaYNxl3QKbpq6KRXIe4W4yvfMssrk71Q+zEwU3lBCK4KEVzEUCApjyINM1qW5ZWRIIPNrTI2Gs9rkWHuQk9Pu3pQkWOGmv1iJJFZkUBzbkWc+bwPRlFEdZCYUblsCgYEA0V0AiVrk5GcA85CXUT2gQw9qDv8rrGhsn5jhk2KqXTPsH78KtNf2SO+ftuZqdzfoDeZHqFkf9C18AC5kkc0ZC7h0vSDxJ0a1ApIHh6Q1fHSq55Vx6JfraBGVTcIWJdrzAsgGVCBmMm/0aNoVYiY7gPTEkxKmKDP0RPe7+l7x93cCgYB3f9+Q7p2hPQ8LWbhizHMokKUzPxTVm+wmhJh5PwdMTKCuWx+qNiNKyac+FggZWoqemFGXwWkYeEjfOWof/ycA5GG/HszgSsZTh96qpnQHykA06TL3jTL6tdCy8/vWRRI6Y2QGoiArVQWtIY67CXO+g8j/orya5n3DU/eVftsf2QKBgFs3AsnVIQ5l1iNH5rVNfSpaZ+HayKNX56NtDzKQdRsgSQrXyAlcYgAK7nHYV6BTajXME4JEOki/HFxT2hsv2W6i0bGozYhIJQrWppMxNuBfUCqPkM4+iiCQMJVtLwsVJnUawq8aYows/LZF9bUKZThc69wL7vprEEyQBoUhopgnAoGAbknca82oSxLhtRWRi1SUe3sx1h7A91HzVG+JWBqdL5KB10DHv6EzLZ6ogp3jJd8Jr90LQaCEGxz1+oqANNpEk4XFSOCxV+SLlieCO66IVsDWlS/MDuiK0zybeZMbcAD+ejlVcD1EeVZP6eXWNRV6+3Zh+d6wR5cVMA5NgczJksA=";
    
    order.partner = partner; //商家appID
    order.seller = seller; //商家支付宝帐号
    order.tradeNO = @""; //订单ID（由商家自行制定）
    order.productName = @""; //商品标题
    order.productDescription = @""; //商品描述
    order.amount = @""; //商品价格
    order.notifyURL =  @""; //回调URL
    
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showUrl = @"m.alipay.com";
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = @"alisdkdemo";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    NSLog(@"orderSpec = %@",orderSpec);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"reslut = %@",resultDic);
        }];
    }

}
@end
