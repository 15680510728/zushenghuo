//
//  MyImagePicker.h
//  iZhangMaiShop
//
//  Created by David on 15/8/29.
//  Copyright (c) 2015年 ZhangMai. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, ImagePickType) {
    ImagePickTypeCamera = 0,
    ImagePickTypePhotoLibrary
};
typedef void(^ImagePickComplete)(UIImage* img);

@interface MyImagePicker : NSObject
+ (MyImagePicker *)sharedMyImagePicker;
-(UIImagePickerController*)pickImageWithImagePickType:(ImagePickType)imagePickType completeAction:(ImagePickComplete)completeAction;
@end
