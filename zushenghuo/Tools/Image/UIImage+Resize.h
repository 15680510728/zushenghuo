//
//  UIImage+Resize.h
//  iZhangMaiShop
//
//  Created by David on 15/10/20.
//  Copyright © 2015年 ZhangMai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage(Resize)
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize ;
@end
