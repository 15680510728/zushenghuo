//
//  MyImagePicker.m
//  iZhangMaiShop
//
//  Created by David on 15/8/29.
//  Copyright (c) 2015年 ZhangMai. All rights reserved.
//
#pragma mark - 单例的宏
#define SYNTHESIZE_SINGLETON_FOR_CLASS(classname) \
\
static classname *shared##classname = nil; \
\
+ (classname *)shared##classname \
{ \
@synchronized(self) \
{ \
if (shared##classname == nil) \
{ \
shared##classname = [[self alloc] init]; \
} \
} \
\
return shared##classname; \
} \
\
+ (id)allocWithZone:(NSZone *)zone \
{ \
@synchronized(self) \
{ \
if (shared##classname == nil) \
{ \
shared##classname = [super allocWithZone:zone]; \
return shared##classname; \
} \
} \
\
return nil; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
return self; \
}


#import "MyImagePicker.h"
#import "UIImage+FixOrientation.h"
#import "UIImage+Resize.h"

@interface MyImagePicker()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(strong,nonatomic)UIImagePickerController* imagePicker;
@property(strong,nonatomic)ImagePickComplete complete;
@end;


@implementation MyImagePicker
SYNTHESIZE_SINGLETON_FOR_CLASS(MyImagePicker);

-(UIImagePickerController*)pickImageWithImagePickType:(ImagePickType)imagePickType completeAction:(ImagePickComplete)completeAction{
    _complete = completeAction;
    if (imagePickType == ImagePickTypeCamera) {
        UIImagePickerController *contr = [[UIImagePickerController alloc] init];
        contr.delegate = self;
        // 拍照
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            NSLog(@"fail");
            return nil;
        } else {
            contr.sourceType = UIImagePickerControllerSourceTypeCamera;
//            contr.allowsEditing = YES;
        }
        return contr;
    }else{
        UIImagePickerController *contr = [[UIImagePickerController alloc] init];
        contr.delegate = self;
        // 相册
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            NSLog(@"fail");
            return nil;
        } else {
            contr.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//            contr.allowsEditing = YES; 
        }
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];//statusBar设置成
        return contr;
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    image = [UIImage fixOrientation:image];
//    CGRect cropRect=[[info objectForKey:@"UIImagePickerControllerCropRect"]CGRectValue];
//    CGImageRef ref = CGImageCreateWithImageInRect(image.CGImage, cropRect);
//    UIImage* cropImage = [UIImage imageWithCGImage:ref];
//    CGImageRelease(ref);
    
    UIImage* temp = [UIImage imageWithImage:[UIImage imageWithData:UIImageJPEGRepresentation(image, 0.3)] scaledToSize:CGSizeMake(800, 800)];
    if (_complete) {
        _complete(temp);
    }
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];//statusBar设置成白色
    [picker dismissViewControllerAnimated:YES completion:^{}];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];//statusBar设置成白色
    [picker dismissViewControllerAnimated:YES completion:^{}];
}
@end
