//
//  UIIamge+FixOrientation.h
//  iZhangMaiShop
//
//  Created by David on 15/9/18.
//  Copyright © 2015年 ZhangMai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage(FixOrientation)
+ (UIImage *)fixOrientation:(UIImage *)aImage;
@end
