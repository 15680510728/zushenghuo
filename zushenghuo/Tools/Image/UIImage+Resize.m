//
//  UIImage+Resize.m
//  iZhangMaiShop
//
//  Created by David on 15/10/20.
//  Copyright © 2015年 ZhangMai. All rights reserved.
//

#import "UIImage+Resize.h"
#import <UIKit/UIKit.h>
@implementation UIImage(Resize)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
