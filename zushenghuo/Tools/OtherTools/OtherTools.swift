//
//  OtherTools.swift
//  zushenghuo
//
//  Created by 李清发 on 15/12/23.
//  Copyright © 2015年 DavidDay. All rights reserved.
//

import Foundation

class OtherTools: NSObject {
   
    //MARK:验证手机号
    class func isMobileNumber(mobileNumber str:String) -> Bool {
        let PHONENUMBER = "^1[3|4|5|6|7|8][0-9]\\d{8}$"
        let regextestmobile = NSPredicate(format: "SELF MATCHES %@", PHONENUMBER)
        if regextestmobile.evaluateWithObject(str) == true{
            return true
        }
        return false
    }
    //MARK:验证密码
    class func isPasswordFormate(password str:String) -> Bool {
        let PASSWORD = "\\w{6,16}"
        let regextestpassword = NSPredicate(format: "SELF MATCHES %@", PASSWORD)
        if regextestpassword.evaluateWithObject(str) == true{
            return true
        }
        return false
    }
    //MARK:存储登录有效期
    class func saveExpiresDate(expiresIn date:Double) {
        let dateExpires = NSDate().timeIntervalSince1970 + date
        NSUserDefaults.standardUserDefaults().setDouble(dateExpires, forKey: kExpiresDate)
    }
    //MARK:验证登录是否过期
    class func didLoginExpiration() -> Bool {
        let dateNow = NSDate().timeIntervalSince1970
        let dateExpires = NSUserDefaults.standardUserDefaults().doubleForKey(kExpiresDate)
        if dateNow > dateExpires {
            return true
        }else{
            return false
        }
    }
}