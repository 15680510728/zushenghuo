//
//  MyRequest.m
//  DDCacheDemo
//
//  Created by David on 15/8/23.
//  Copyright (c) 2015年 DavidDay. All rights reserved.
//

#import "MyRequest.h"
#define SERVER_HOST @"http://116.231.228.172:8001"
#define kAccessToken @"accesstoken"
@implementation MyRequest

+(void)PostWithUrl:(NSString*)url
        parameters:(id)parameters
          response:(MyRequestResponse)response{
        AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
    NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:kAccessToken];
    if (token) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"JWT %@",token] forHTTPHeaderField:@"Authorization"];
    }else{
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"JWT"] forHTTPHeaderField:@"Authorization"];
    }
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        if (operation.response.statusCode == 200) {//正常返回
            id result = responseObject;
//            id result = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            if ([result isKindOfClass:[NSDictionary class]]) {
                NSNumber* code = [result valueForKeyPath:@"code"];
                if (code) {//可能错误
                    if ([code isEqual:@1]) {//绝逼错误
                        response(operation,nil,@"返回失败",MyRequestResponseCodeFailed);
                    }else{
                        //还不知道有没别的接口可能有code 但是是正确返回的
                    }
                }else{//这里应该返回正确
                    response(operation,result,@"返回成功",MyRequestResponseCodeSuccess);
                }
            }else{
                response(operation,nil,@"返回失败",MyRequestResponseCodeFailed);
            }
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        response(operation,@{},@"网络错误",MyRequestResponseCodeFailed);
    }];
}


#pragma mark - 登陆
+(void)loginWithPhone:(NSString*)phone
             password:(NSString*)password
             response:(MyRequestResponse)response{
    NSDictionary* parameters = @{@"grant_type":@"password",@"username":phone,@"password":password};
    [MyRequest PostWithUrl:[SERVER_HOST stringByAppendingString:@"/auth"] parameters:parameters response:response];
}

#pragma mark - 获取验证码
+(void)pinWithPhone:(NSString*)phone
             response:(MyRequestResponse)response{
    NSDictionary* parameters = @{@"phone":phone};
    [MyRequest PostWithUrl:[SERVER_HOST stringByAppendingString:@"/v1/pin"] parameters:parameters response:response];
}

#pragma mark - 注册
+(void)registerWithPhone:(NSString*)phone
                     pin:(NSString*)pin
                password:(NSString*)password
           response:(MyRequestResponse)response{
    NSDictionary* parameters = @{@"phone":phone,@"pin":pin,@"password":password};
    [MyRequest PostWithUrl:[SERVER_HOST stringByAppendingString:@"/v1/register"] parameters:parameters response:response];
}

@end
