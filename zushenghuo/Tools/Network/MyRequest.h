//
//  MyRequest.h
//  DDCacheDemo
//
//  Created by David on 15/8/23.
//  Copyright (c) 2015年 DavidDay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
typedef NS_ENUM(NSInteger, MyRequestResponseCode) {
    MyRequestResponseCodeSuccess = 520,
    MyRequestResponseCodeFailed,
};
typedef void(^MyRequestResponse)(AFHTTPRequestOperation *operation, id responseObject, NSString *responseMsg,MyRequestResponseCode responseCode);

@interface MyRequest : NSObject

+(void)loginWithPhone:(NSString*)phone
             password:(NSString*)password
             response:(MyRequestResponse)response;

+(void)pinWithPhone:(NSString*)phone
           response:(MyRequestResponse)response;

+(void)registerWithPhone:(NSString*)phone
                     pin:(NSString*)pin
                password:(NSString*)password
                response:(MyRequestResponse)response;
@end